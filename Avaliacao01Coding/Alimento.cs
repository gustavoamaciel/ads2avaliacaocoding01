﻿using System;

namespace Domain
{
    public class Alimento
    {
        private string nome;
        private string origem;

        public string Nome { get => nome; set => nome = value; }
        public string Origem { get => origem; set => origem = value; }

        public Alimento(string nome, string origem)
        {
            Nome = nome;
            Origem = ChecarOrigem(origem);
        }

        public virtual void ExibirInformacoes()
        {
            Console.Write($"O nome desse Alimento é {Nome}, tem origem {Origem}");
        }

        private static string ChecarOrigem(string origem)
        {
            if (origem != "Animal" && origem != "Vegetal" && origem != "Mineral")
            {
                return "Desconhecido";
            }

            return origem;
        }

    }
}
