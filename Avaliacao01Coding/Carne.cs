﻿using System;

namespace Domain
{
    public class Carne : Alimento
    {
        private string tipo;
        private string animal;
        private bool consumirCrua;

        public string Tipo { get => tipo; set => tipo = value; }
        public string Animal { get => animal; set => animal = value; }
        public bool ConsumirCrua { get => consumirCrua; set => consumirCrua = value; }

        public Carne(string tipo, string animal, bool consumirCrua, string nome, string origem) : base(nome, origem)
        {
            Tipo = tipo;
            Animal = animal;
            ConsumirCrua = consumirCrua;
        }

        private string ChecarSePodeConsumir()
        {
            if (ConsumirCrua) {
                return "pode consumir crua.";
            }
            return "não pode consumir crua.";
        }

        public override void ExibirInformacoes()
        {
            base.ExibirInformacoes();
            Console.Write($" sendo do tipo Carne. É uma {Tipo} que veio de um animal do tipo {Animal} e {ChecarSePodeConsumir()}");
        }
    }
}
