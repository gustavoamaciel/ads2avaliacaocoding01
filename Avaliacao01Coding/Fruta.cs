﻿using System.Collections.Generic;
using System;
using System.Linq;

namespace Domain
{
    public class Fruta : Alimento
    {
        private List<string> vitaminas = new List<string>();
        private string cor;
        private string nivelAcidez;

        public List<string> Vitaminas { get => vitaminas; }
        public string Cor { get => cor; set => cor = value; }
        public string NivelAcidez { get => nivelAcidez; set => nivelAcidez = value; }

        public Fruta( string cor, string nivelAcidez, string nome, string origem) :base(nome, origem)
        {
            Cor = cor;
            NivelAcidez = nivelAcidez;
        }

        public void AdicionarVitaminas(string vitamina)
        {
            if (vitamina == "")
            {
                Console.WriteLine("O valor da vitamina não pode ser vazio");
            } else
                 Vitaminas.Add(vitamina);
        }


        // Sobrecarga para adicionar lista em vez de uma só vitamina
        public void AdicionarVitaminas(string[] vitaminas)
        {
            if (vitaminas.Length == 0 || vitaminas == null)
            {
                Console.Write("Os valores das vitaminas não podem ser vazio.");
            } else
                Vitaminas.AddRange(vitaminas);
        }

        public void CorDoConsoleDependendoDaFruta()
        {
            switch (Cor) {
                case "Vermelha":
                    Console.ForegroundColor = ConsoleColor.Red;
                    break;
                case "Verde":
                    Console.ForegroundColor = ConsoleColor.Green;
                    break;
                case "Amarelo":
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    break;
                default:
                    break;
            }
        }

        private void ExibirVitaminas()
        {
            foreach(var vitamina in Vitaminas)
            {
                if (vitamina == Vitaminas.Last())
                {
                    Console.Write($"{vitamina}.");
                } else Console.Write($"{vitamina}, ");
            }
        }

        public override void ExibirInformacoes()
        {
            base.ExibirInformacoes();
            Console.Write($" sendo do tipo Fruta. Sua cor é {Cor} e seu nível de acidez é {NivelAcidez} e suas vitaminas são ");
            ExibirVitaminas();
        }
    }
}
