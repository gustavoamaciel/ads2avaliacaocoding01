﻿using System;

namespace Domain
{
    public class Pao : Alimento
    {
        private double calorias;
        private double carboidratos;
        private double proteinas;


        public double Calorias { get => calorias; set => calorias = value; }
        public double Carboidratos { get => carboidratos; set => carboidratos = value; }
        public double Proteinas { get => proteinas; set => proteinas = value; }

        public Pao(double calorias, double carboidratos, double proteinas, string nome, string origem) : base(nome, origem)
        {
            Calorias = calorias;
            Carboidratos = carboidratos;
            Proteinas = proteinas;
        }


        // Sobrecarga construtor
        public Pao(double carboidratos, double proteinas, string nome, string origem) : base(nome, origem)
        {
            Console.WriteLine($"{Nome} está sem quantidade de calorias.");
            Carboidratos = carboidratos;
            Proteinas = proteinas;
        }

        public void AvisoCalorias()
        {
            if (Calorias > 1000)
            {
                Console.WriteLine($"Este {Nome} tem mais de 1000 calorias, cuidado!");
            }
        }

        public override void ExibirInformacoes()
        {
            base.ExibirInformacoes();
            Console.Write($" sendo do tipo Pão. O {Nome} tem {Calorias} calorias, {carboidratos} carboidratos e {Proteinas} proteínas.");
        }

        public void AssarPao()
        {
            Console.WriteLine($"Começando os preparativos do {Nome}...");
            Console.WriteLine("Preparando massa...");
            Console.WriteLine("Amassando a massa...");
            Console.WriteLine("Formando a massa...");
            Console.WriteLine("Assando a massa...");
            Console.WriteLine($"Seu {Nome} está pronto!!!");
        }
    }
}
