﻿using System;
using Domain;

namespace Avaliacao01Coding
{
    class Program
    {
        static void Main(string[] args)
        {
            Fruta f1 = new Fruta("Vermelha", "Alta", "Morango", "Vegetal");
            Pao p1 = new Pao(310, 52, 11, "Pão Francês", "Vegetal");
            Carne c1 = new Carne("Carne Branca", "Peixe", true, "Salmão", "Animal");

            string[] vitaminas = { "Vitamina E", "Vitamina C" };

            f1.AdicionarVitaminas(vitaminas);

            f1.ExibirInformacoes();
            Console.WriteLine();
            p1.ExibirInformacoes();
            Console.WriteLine();
            c1.ExibirInformacoes();
        }
    }
}
