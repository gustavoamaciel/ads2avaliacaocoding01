# ADS2AvaliaçãoCoding01

A avaliação deverá ser devolvida até amanhã neste mesmo horário de início desta prova, de forma completa.

Leia com atenção o enunciado das questões e as observações.

TODAS as questões serão relacionadas com a lista que segue, e devem ser feitas conforme o tema abaixo, de acordo com a PRIMEIRA LETRA DO SEU PRIMEIRO SOBRENOME:

**Obs¹**.: As questões devem ser enviadas em um arquivo compactado ou em algum link compartilhado (Google Drive, One Drive, GitHub, etc) contendo TODA A SOLUÇÃO E SEUS RESPECTIVOS PROJETOS.
**Obs²**.: Serão aceitos códigos feitos conforme TODAS as instruções, bem como devem estar COMPLETOS. Cuidado com as cópias. Em caso de identificação de cópia de código, ambos os envios serão desconsiderados.
**Obs³**.: Podem ser enviados nas linguagens C#, Java, TypeScript, PHP, C++ ou Python. 

# CONTEXTO:
**A – Registro de tipos de Alimentos diferentes e suas respectivas características.**

# Questões:
Enunciado para todas as questões: De acordo com o tema que foi selecionado para o seu sobrenome, crie um sistema que:

01.  Possua pelo menos 4 classes (pelo menos uma herança e pelo menos um polimorfismo), excluindo a classe program;
02.  Possua seus respectivos atributos (pelo menos 3 para cada classe, excluindo as heranças, que devem ter pelo menos 1 atributo) com seus tipos (identificados corretamente para armazenar a natureza do valor do atributo);
03.  Todos os atributos devem possuir o encapsulamento de dados;
04.  Cada classe deve possuir pelo menos 1 construtor (excluindo o construtor padrão vazio);
05.  Todo o projeto deve possuir pelo menos 6 métodos, distribuídos ao longo de todo o projeto (Exemplo.: Uma classe pode ter 3, outra 2, outra 1 e as demais nenhum método) – Excluindo os construtores;
06.  Pelo menos um dos métodos deve possuir sobrecarga (podendo ser um dos 6 métodos do projeto);
07.  Pelo menos um dos construtores deve possuir sobrecarga;
08.  Na classe Program.cs, crie as respectivas a instâncias das classes (objetos), preenchendo todos os seus respectivos valores.
09.  Crie um método responsável pela exibição das informações em cada uma das classes (excluindo os métodos anteriores - 6 métodos do projeto);
10.  Chame o método criado na questão anterior para cada um dos objetos instanciados;
